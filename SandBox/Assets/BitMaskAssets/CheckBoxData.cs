﻿using UnityEngine;

[CreateAssetMenu(fileName = "Checkbox Data")]
public class CheckBoxData : ScriptableObject
{
    public int CheckBox = 0;
}
