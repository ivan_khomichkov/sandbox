﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
public class ToggleController : MonoBehaviour
{
    private Toggle toggle;

    [SerializeField] private int toggleIndex;

    public int ToggleIndex => toggleIndex;

    private bool firstInit = false;
    // Use this for initialization
	void Start ()
	{
	    firstInit = true;
	    toggle = GetComponent<Toggle>();
	    toggle.GetComponentInChildren<Text>().text = toggleIndex.ToString();
	    toggle.isOn = InfoCollector.I.GetCheckboxValue(toggleIndex);
	    firstInit = false;
	}
	
    public void OnValueChanged()
    {
        if (!firstInit)
        {
            InfoCollector.I.SwitchCheckboxValue(toggleIndex);
        }
    }
}
