﻿using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

internal class InfoCollector : MonoBehaviour
{
    public static InfoCollector I;

    private void Awake()
    {
        I = this;
        PerformTests();
    }

    [SerializeField] private InputField binResult;
    [SerializeField] private InputField decResult;
    [SerializeField] private CheckBoxData data;

    public bool GetCheckboxValue(int checkBoxNumber)
    {
        var result = GetBinByIndex(data.CheckBox, checkBoxNumber);
        UpdateInputFields();
        return result;
    }

    public void SwitchCheckboxValue(int checkboxNumber)
    {
        data.CheckBox = SwitchBinAtIndex(data.CheckBox, checkboxNumber);
        UpdateInputFields();
    }

    private bool GetBinByIndex(int index, int checkBoxNumber)
    {
        return (index & 1 << checkBoxNumber) != 0;
    }

    private int SwitchBinAtIndex(int value, int index)
    {
        return value ^= 1 << index;
    }

    private string BinToString(int value, int numberOfBits = 8)
    {
        int tempData = value;
        StringBuilder myStr = new StringBuilder();
        while (tempData > 0)
        {
            myStr.Append(tempData % 2);
            tempData /= 2;
        }

        if (myStr.Length < numberOfBits)
        {
            myStr.Append(new string('0', numberOfBits - myStr.Length));
        }
        var zeroAndOneArray = myStr.ToString().ToCharArray();
        char[] result = zeroAndOneArray.Reverse().ToArray();
        return string.Concat(result);
    }

    private void UpdateInputFields()
    {
        if (binResult != null) binResult.text = BinToString(data.CheckBox);
        if (decResult != null) decResult.text = data.CheckBox.ToString();
    }

    private void PerformTests()
    {
        Debug.Assert(BinToString(3) == "00000011");
        Debug.Assert(BinToString(2) == "00000010");
        Debug.Assert(BinToString(1) == "00000001");
        Debug.Assert(BinToString(14) == "00001110");
    }
}
