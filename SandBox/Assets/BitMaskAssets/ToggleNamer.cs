﻿using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(ToggleController))]
public class ToggleNamer : MonoBehaviour
{
   
    // Use this for initialization
    void Update()
    {
        name = "Toggle[" + GetComponent<ToggleController>().ToggleIndex + "]";
    }
}
